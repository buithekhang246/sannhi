import json
import os
import time
from builtins import print
import datetime
import mariadb
import requests
from piny import YamlLoader, StrictMatcher
import threading

cwd = os.getcwd()  # Get the current working directory (cwd)

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()
url_call = config_yml['url_base']

itechConfig = {
    'host': config_yml['databases_itech']['host'],
    'port': config_yml['databases_itech']['port'],
    'user': config_yml['databases_itech']['user'],
    'password': config_yml['databases_itech']['pass'],
    'database': config_yml['databases_itech']['database'],
}

sql_user = 'select hu.id ,hu.username  FROM hosp_user hu'

sql_report = "SELECT ho.accession_number  ,hr.transcriber_id   " \
             ",hr.note,hr.body_html ,hr.conclusion_html,hr.body_rtf   " \
             ",hr.conclusion_rtf ,hr.hospital_id ,hr.created_time ,hr.creator_id ,   " \
             "hr.last_updated_time ,hr.last_updated_id ,hr.voided ,hr.voided_time ,hr.voided_by ,hr.void_reason   " \
             "from hosp_report hr left join hosp_order_procedure hop on hr.order_procedure_id=hop.id   " \
             "left join hosp_order ho on hop.order_id =ho.id"

conn_his = mariadb.connect(**itechConfig)
cur_his = conn_his.cursor()

cur_his.execute(sql_user)
results_user = cur_his.fetchall()

data_user = {}
for row in results_user:
    data_user[row[0]] = row

cur_his.execute(sql_report)
results_report = cur_his.fetchall()

data_insert = []

for row in results_report:
    json_insert = {}
    json_insert['accession_number'] = row[0]
    if row[1] is not None:
        json_insert['transcriber_id'] = data_user[row[1]][1]
    else:
        json_insert['transcriber_id'] = None
    json_insert['note'] = row[2]
    json_insert['body_html'] = row[3]
    json_insert['conclusion_html'] = row[4]
    json_insert['body_rtf'] = row[5]
    json_insert['conclusion_rtf'] = row[6]
    json_insert['hospital_id'] = row[7]
    json_insert['created_time'] = row[8]
    if row[9] is not None:
        json_insert['creator_id'] = data_user[row[9]][1]
    else:
        json_insert['creator_id']=None
    json_insert['last_updated_time'] = row[10]
    if row[11] is not None:
        json_insert['last_updated_id'] = data_user[row[11]][1]
    else:
        json_insert['creator_id'] = None
    json_insert['voided'] = row[12]
    json_insert['voided_time'] = row[13]
    json_insert['voided_by'] = row[14]
    json_insert['void_reason'] = row[15]
    data_insert.append(json_insert)
    # print(json_insert)
    # print(json.dumps(json_insert))#
# db new
sql_order_product_db_new = 'SELECT hop.id ,ho.accession_number FROM hosp_order_procedure hop  left join hosp_order ho ' \
                           'on hop.order_id =ho.id '
sql_user_new_db = "select hu.id ,hu.username  FROM hosp_user hu"

conn_his_new = mariadb.connect(**itechConfig)
cur_his_new = conn_his.cursor()

cur_his_new.execute(sql_order_product_db_new)
results_order_product = cur_his_new.fetchall()

data_user = {}
for row in results_user:
    data_user[row[0]] = row

cur_his_new.execute(sql_user_new_db)
results_user_new = cur_his_new.fetchall()
data_user_new = {}
for row in results_user_new:
    data_user_new[row[1]] = row
print(json.dumps(data_user_new))
data_order_product_db_new = {}
for row in results_order_product:
    data_order_product_db_new[row[1]] = row
# set data inser
for json_data in data_insert:
    if data_order_product_db_new.get(json_data['accession_number']) is not None:
        json_data['order_product'] = data_order_product_db_new[json_data['accession_number']][0]
    if data_user_new.get(json_data['transcriber_id']) is not None:
        json_data['transcriber_id_new'] = data_user_new[json_data['transcriber_id']][0]

    if data_user_new.get(json_data['creator_id']) is not None:
        json_data['creator_id_new'] = data_user_new[json_data['creator_id']][0]
    if data_user_new.get(json_data['last_updated_id']) is not None:
        json_data['last_updated_id_new'] = data_user_new[json_data['last_updated_id']][0]


