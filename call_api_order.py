import json
import os
import time
from builtins import print
import datetime
import mariadb
import requests
from piny import YamlLoader, StrictMatcher
import threading
cwd = os.getcwd()  # Get the current working directory (cwd)

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()

itechConfig = {
    'host': config_yml['databases_itech']['host'],
    'port': config_yml['databases_itech']['port'],
    'user': config_yml['databases_itech']['user'],
    'password': config_yml['databases_itech']['pass'],
    'database': config_yml['databases_itech']['database'],
}
url_base = config_yml['url_base']
basic = config_yml['basic']


conn_his = mariadb.connect(**itechConfig)
cur_his = conn_his.cursor()

sql_order = "SELECT ho.id ,\n" \
            "ho.accession_number\n" \
            ",ho.modality_type ,\n" \
            "ho.order_number ,\n" \
            "ho.clinical_diagnosis,\n" \
            "ho.requested_department_code ,\n" \
            "ho.referring_physician_code ,\n" \
            "ho.requested_department_name  ,\n" \
            "ho.referring_physician_name ,\n" \
            "ho.instructions ,\n" \
            "DATE_FORMAT(ho.order_time , '%Y%m%d%T') as order_time,\n" \
            "ho .urgent ,\n" \
            "ho.admission_type_id ,\n" \
            "ho.encounter_number ,\n" \
            "1  ,\n" \
            "ho.insurance_number ,\n" \
            "ho.insurance_issued_date ,\n" \
            "ho.insurance_expired_date,\n" \
            "patient_id\n" \
            "from hosp_order ho   "

sql_patient = 'select hp.id ,hp.initial_secret,hp.address ,hp.pid ,hp .birth_date ,hp.fullname ,' \
              'hp.gender ,hp.phone ,hp.email FROM hosp_patient hp'
sql_services_order = 'SELECT hop.order_id ,hop.procedure_id ,hop.icd_code,hop.requested_number ,' \
                     'hop.requested_proc_code ,' \
                     'hop.requested_proc_name ,hop.requested_modality_type , ' \
                     "DATE_FORMAT(hop.requested_time  , '%Y%m%d%T') as order_time " \
                     'FROM hosp_order_procedure ' \
                     'hop '

s1 = time.time()
cur_his.execute(sql_order)
results_order = cur_his.fetchall()

data_json_patient = {}
cur_his.execute(sql_patient)
results_patient = cur_his.fetchall()
for row in results_patient:
    data_json_patient[str(row[0])] = row

data_json_services_order = {}
cur_his.execute(sql_services_order)
results_services_order = cur_his.fetchall()
for row in results_services_order:
    data_json_services_order[str(row[0])] = row

# print(results_order)
# print(data_json_patient)
list_data_api_call = []
for row in results_order:
    data_json_call = {}
    data_json_call['orderNumber'] = row[1]
    data_json_call['modalityType'] = row[2]
    data_json_call['accessionNumber'] = row[3]
    data_json_call['clinicalDiagnosis'] = row[4]
    data_json_call['requestedDepartmentCode'] = row[5]
    data_json_call['referringPhysicianCode'] = row[6]
    data_json_call['requestedDepartmentName'] = row[7]
    data_json_call['referringPhysicianName'] = row[8]
    data_json_call['instructions'] = row[9]
    data_json_call['orderDatetime'] = str(row[10]).replace(':', '')
    data_json_call['attributes'] = None

    data_json_call['urgent'] = row[11]
    data_json_call['admission_type_id'] = row[12]
    data_json_call['encounter_number'] = row[13]
    # data_json_call['insurance_applied'] = row[14]
    data_json_call['insurance_number'] = row[15]
    data_json_call['insurance_issued_date'] = row[16]
    data_json_call['insurance_expired_date'] = row[17]

    if data_json_services_order.get(str(row[0])) is not None:
        data_service = {}
        data_service['icdCode'] = data_json_services_order[str(row[0])][2]
        data_service['requestedNumber'] = data_json_services_order[str(row[0])][3]
        data_service['requestedProcedureCode'] = data_json_services_order[str(row[0])][4]
        data_service['requestedProcedureName'] = data_json_services_order[str(row[0])][5]
        data_service['requestedModalityType'] = data_json_services_order[str(row[0])][6]
        data_service['requestedDatetime'] = str(data_json_services_order[str(row[0])][7]).replace(':', '')
        data_list = []
        data_list.append(data_service)
        data_json_call['services'] = data_list
    if data_json_patient.get(str(row[18])) is not None:
        data_patient = {}
        data_patient['initialSecret'] = data_json_patient[str(row[18])][1]
        data_patient['address'] = data_json_patient[str(row[18])][2]

        data_patient['pid'] = data_json_patient[str(row[18])][3]
        data_patient['birthDate'] = data_json_patient[str(row[18])][4]

        data_patient['fullname'] = data_json_patient[str(row[18])][5]
        data_patient['gender'] = data_json_patient[str(row[18])][6]
        data_patient['phone'] = data_json_patient[str(row[18])][7]

        data_patient['email'] = data_json_patient[str(row[18])][8]
        data_json_call['patient'] = data_patient
    list_data_api_call.append(data_json_call)

index_file = len(list_data_api_call)

start_time=time.time()
def call_api(index_start, index_end, thread_index):
    json_error = []
    total = 0
    i_z = 0
    for i in range(index_start, index_end):
        url = url_base + "/conn/ws/rest/v1/hospital/31313/order"
        row = list_data_api_call[i]
        s1 = time.time()

        print(json.dumps(row))
        x = requests.post(url, data=json.dumps(row),
                          headers={"Authorization": "Basic " + basic,
                                   "Content-Type": "application/json"})
        print("Call api takes %s" % (time.time() - s1) + "thread " + str(thread_index))
        total = total + 1
        print(x.content)
        if x.status_code != 200 :
            # print(row)
            i_z = i_z + 1
            json_error.append(row)
            cwd = os.getcwd()  # Get the current working directory (cwd)
            files = str(cwd) + "/study/error_study" + str(thread_index) + ".txt"
            os.makedirs(os.path.dirname(files), exist_ok=True)
            with open(files, 'w+') as outFile:
                outFile.write(json.dumps(json_error))
        print("total error " + str(i_z) + "/" + str(total) + "thread" + str(thread_index))


print("--- %s seconds2 end thread---" % (time.time() - start_time))

print("--- %s end end thread---" % (time.time() - start_time))
try:
    threads = []

    thread = 10
    index_row = int(index_file / thread)
    for imdex in range(thread):
        if imdex == thread - 1:
            t1 = threading.Thread(target=call_api, args=((thread - 1) * index_row, index_file, imdex))
            t1.start()
            threads.append(t1)
        else:
            t1 = threading.Thread(target=call_api, args=(imdex * index_row, index_row * (imdex + 1) - 1, imdex))
            t1.start()
            threads.append(t1)
    for thread in threads:
        thread.join()

except:
    print("Error: unable to start thread")
