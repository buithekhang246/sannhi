import json
import os
import time
from builtins import print
import datetime
import mariadb
import requests
from piny import YamlLoader, StrictMatcher
import threading

cwd = os.getcwd()  # Get the current working directory (cwd)

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()
url_call = config_yml['url_base']

itechConfig = {
    'host': config_yml['databases_itech']['host'],
    'port': config_yml['databases_itech']['port'],
    'user': config_yml['databases_itech']['user'],
    'password': config_yml['databases_itech']['pass'],
    'database': config_yml['databases_itech']['database'],
}
url_base = config_yml['url_base']
basic = config_yml['basic']

conn_his = mariadb.connect(**itechConfig)
cur_his = conn_his.cursor()

sql_study = "SELECT   " \
            "hs.tag_accession_number  " \
            ",hs.study_iuid ,  " \
            "DATE_FORMAT(hs.study_time , '%T')       as studyTime,  " \
            "DATE_FORMAT(hs.study_time, '%Y%m%d') as studyDate,  " \
            "hs.tag_body_part_examined,  " \
            "hs.tag_referring_physician ,  " \
            "hs.tag_description ,  " \
            "hs.tag_modality_type ,  " \
            "hs.tag_operator_name ,  " \
            "hs.tag_gender ,  " \
            "hs.tag_birth_date ,  " \
            "hs.tag_pid ,  " \
            "hs.tag_patient_name ,  " \
            "hs.num_of_images ,  " \
            "hs.num_of_series ,  " \
            "hs.modality_id   " \
            "FROM hosp_study hs  limit 30 "

sql_modality = 'select hm.id ,hm.ae_title ,hm.ip_address ,hm.institution_name ,hm.model_name ,hm.station_name ' \
               ' FROM hosp_modality hm '
s1 = time.time()
cur_his.execute(sql_study)
results_study = cur_his.fetchall()

cur_his.execute(sql_modality)
results_modality = cur_his.fetchall()
data_modality = {}
for row in results_modality:
    data_modality[str(row[0])] = row

list_data_api_call = []

for row in results_study:
    data_json = {}
    data_json['accessionNumber'] = row[0]
    data_json['studyInstanceUID'] = row[1]
    data_json['studyTime'] = row[2]
    data_json['studyDate'] = row[3]
    data_json['bodyPartExamined'] = row[4]
    data_json['referringPhysician'] = row[5]
    data_json['description'] = row[6]
    data_json['modalityType'] = row[7]
    data_json['operatorName'] = row[8]
    data_json['gender'] = row[9]
    data_json['birthDate'] = row[10]
    data_json['patientId'] = row[11]
    data_json['patientName'] = row[12]
    data_json['numOfImages'] = row[13]
    data_json['numOfSeries'] = row[14]
    if data_modality.get(str(row[15])) is not None:
        data_json['aeTitle'] = data_modality.get(str(row[15]))[1]
        data_json['ipAddress'] = data_modality.get(str(row[15]))[2]
        data_json['institutionName'] = data_modality.get(str(row[15]))[3]
        data_json['manufacturerModelName'] = data_modality.get(str(row[15]))[4]
        data_json['stationName'] = data_modality.get(str(row[15]))[5]
    list_data_api_call.append(data_json)

print(json.dumps(list_data_api_call))

index_file = len(list_data_api_call)

start_time = time.time()


def call_api(index_start, index_end, thread_index):
    json_error = []
    total = 0
    i_z = 0
    for i in range(index_start, index_end):

        url = url_base + "/conn/ws/rest/v1/hospital/31313/study"
        row = list_data_api_call[i]
        s1 = time.time()
        print(json.dumps(row))
        # x = requests.post(url, data=json.dumps(row),
        #                   headers={"Authorization": "Basic " + basic,
        #                            "Content-Type": "application/json"})
        # print("Call api takes %s" % (time.time() - s1) + "thread " + str(thread_index))
        # total = total + 1
        # if x.status_code != 200 or json.loads(x.content)['header']['code'] != 200:
        #     # print(row)
        #     i_z = i_z + 1
        #     json_error.append(row)
        #     cwd = os.getcwd()  # Get the current working directory (cwd)
        #     files = str(cwd) + "/study/error_study" + str(thread_index) + ".txt"
        #     os.makedirs(os.path.dirname(files), exist_ok=True)
        #     with open(files, 'w+') as outFile:
        #         outFile.write(json.dumps(json_error))
    print("total error " + str(i_z) + "/" + str(total) + "thread" + str(thread_index))


print("--- %s seconds2 end thread---" % (time.time() - start_time))

print("--- %s end end thread---" % (time.time() - start_time))
try:
    threads = []

    thread = 10
    index_row = int(index_file / thread)
    for imdex in range(thread):
        if imdex == thread - 1:
            t1 = threading.Thread(target=call_api, args=((thread - 1) * index_row, index_file, imdex))
            t1.start()
            threads.append(t1)
        else:
            t1 = threading.Thread(target=call_api, args=(imdex * index_row, index_row * (imdex + 1) - 1, imdex))
            t1.start()
            threads.append(t1)
    for thread in threads:
        thread.join()

except:
    print("Error: unable to start thread")
