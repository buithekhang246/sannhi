import json
import os
import time
from builtins import print
import datetime
import mariadb
import requests
from piny import YamlLoader, StrictMatcher
import threading

cwd = os.getcwd()  # Get the current working directory (cwd)

config_yml = YamlLoader(path=str(cwd) + "/config.yaml", matcher=StrictMatcher).load()
url_call = config_yml['url_base']

itechConfig = {
    'host': config_yml['databases_itech']['host'],
    'port': config_yml['databases_itech']['port'],
    'user': config_yml['databases_itech']['user'],
    'password': config_yml['databases_itech']['pass'],
    'database': config_yml['databases_itech']['database'],
}

itechConfig_new = {
    'host': config_yml['databases_itech_new']['host'],
    'port': config_yml['databases_itech_new']['port'],
    'user': config_yml['databases_itech_new']['user'],
    'password': config_yml['databases_itech_new']['pass'],
    'database': config_yml['databases_itech_new']['database'],
}

sql_hopspots_insert = "INSERT INTO syst_hospital\n" \
                      "(id, name, name_search, description, phone, email, address, config_json, enabled, created_time, creator_id, last_updated_time, last_updated_id, voided, voided_time, voided_by, void_reason)\n" \
                      "VALUES('31313', 'Bệnh viện Đa khoa Quốc tế Hải Phòng', 'benh vien da khoa quoc te hai phong', NULL, '02253955888', NULL, '124 P. Nguyễn Đức Cảnh, Cát Dài, Lê Chân, Hải Phòng', '{\"signEnabled\":\"true\",\"hisEnabled\":\"true\"}', 1, '2022-01-13 11:52:18.000', 1, '2022-03-05 15:29:06.000', 1, 0, NULL, NULL, NULL);\n"
sql_hopspots_insert_2 = "INSERT INTO syst_hospital\n" \
                        "(id, name, name_search, description, phone, email, address, config_json, enabled, created_time, creator_id, last_updated_time, last_updated_id, voided, voided_time, voided_by, void_reason)\n" \
                        "VALUES('31313-002', 'Bệnh viện Quốc tế Sản nhi Hải Phòng', 'benh vien quoc te san nhi hai phong', NULL, '02253955888', NULL, '124 P. Nguyễn Đức Cảnh, Cát Dài, Lê Chân, Hải Phòng', '{\"signEnabled\":\"true\",\"hisEnabled\":\"true\"}', 1, '2022-01-13 11:52:18.000', 1, '2022-03-19 16:23:02.000', 1, 0, NULL, NULL, NULL);"

print(itechConfig_new)
sql_select_department = "select  id,parent_id, code, name, description, enabled, hospital_id, created_time, creator_id, " \
                        "last_updated_time," \
                        " last_updated_id from hosp_department "

sql_dep = "INSERT INTO hosp_department" \
          "(id, parent_id, code, name, description, enabled, hospital_id, created_time, creator_id, last_updated_time," \
          " last_updated_id)" \
          "VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s)"

conn_his = mariadb.connect(**itechConfig)
cur_his = conn_his.cursor()

conn_his_new = mariadb.connect(**itechConfig_new)
cur_his_new = conn_his_new.cursor()
try:
    cur_his_new.execute(sql_hopspots_insert)
    cur_his_new.execute(sql_hopspots_insert_2)
    conn_his_new.commit()
except:
    print("error")

cur_his.execute(sql_select_department)
results_select_department = cur_his.fetchall()
let_data_department = []
i = 1
parent_id_list = {}
i = 1

print(results_select_department)
data_insert_value = {}
for row in results_select_department:
    as_list = list(row)
    as_list[0] = i
    as_list[8] = 1
    as_list[10] = 1
    i = i + 1
    data_insert_value[row[0]] = tuple(as_list)

print(data_insert_value)
let_data_department_insert = []

for row_1 in data_insert_value.keys():
    print(row_1)
    row = data_insert_value[row_1]
    as_list = list(row)
    as_list[1] = data_insert_value.get(as_list[1])[0]
    print(as_list)
    let_data_department_insert.append(tuple(as_list))

# save_db_new
# print(let_data_department_insert)
# print(parent_id_list)

try:
    cur_his_new.executemany(sql_dep, let_data_department_insert)
    conn_his_new.commit()
except:
    print("errrpr")

sql_role_type = 'select id, name, created_time, creator_id, last_updated_time, last_updated_id from syst_role_type'

sql_role_type_insert = "INSERT INTO syst_role_type" \
                       "(id, name, created_time, creator_id, last_updated_time, last_updated_id)" \
                       "VALUES(%s, %s, %s, %s, %s, %s);"
cur_his.execute(sql_role_type)
results_role_type = cur_his.fetchall()
let_data_role_type = []
for row in results_role_type:
    as_list = list(row)
    as_list[3] = 1
    as_list[5] = 1
    let_data_role_type.append(tuple(as_list))

# save_db_new

try:
    cur_his_new.executemany(sql_role_type_insert, let_data_role_type)
    conn_his_new.commit()
except:
    print("errrpr")

sql_role = 'select id, type_id, name, name_search, description, created_time, creator_id, last_updated_time, ' \
           'last_updated_id from syst_role '

sql_role_insert = "INSERT INTO syst_role" \
                  "(id, type_id, name, name_search, description, created_time, creator_id, last_updated_time, " \
                  "last_updated_id)" \
                  "VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s);"
cur_his.execute(sql_role)
results_role = cur_his.fetchall()
let_data_role = []
for row in results_role:
    as_list = list(row)
    as_list[6] = 1
    as_list[8] = 1
    let_data_role.append(tuple(as_list))
print(let_data_role)
# save_db_new

try:
    cur_his_new.executemany(sql_role_insert, let_data_role)
    conn_his_new.commit()
except:
    print("errrpr")

sql_sys_modality_addy = 'select id, description, created_time, creator_id, last_updated_time, last_updated_id from ' \
                        'syst_modality_abbr '
sql_sys_modality_insert = 'INSERT INTO syst_modality_abbr' \
                          "(id, description, created_time, creator_id, last_updated_time, last_updated_id)" \
                          "VALUES(%s, %s, %s, %s, %s, %s);"
cur_his.execute(sql_sys_modality_addy)
results_role = cur_his.fetchall()
let_data_sys_modality_addy = []
for row in results_role:
    as_list = list(row)
    as_list[3] = 1
    as_list[5] = None
    let_data_sys_modality_addy.append(tuple(as_list))

print(let_data_sys_modality_addy)

# save_db_new

try:
    cur_his_new.executemany(sql_sys_modality_insert, let_data_sys_modality_addy)
    conn_his_new.commit()
except:
    print("errrpr")

# Group

sql_group = 'select  name, `level`, description, hospital_id, created_time, creator_id, last_updated_time, ' \
            'last_updated_id from ' \
            'hosp_group '
sql_group_insert = "INSERT INTO hosp_group" \
                   "( name, `level`, description, hospital_id, created_time, creator_id, last_updated_time, " \
                   "last_updated_id)" \
                   "VALUES( %s, %s, %s, %s, %s, %s,%s,%s);"

cur_his.execute(sql_group)
results_role = cur_his.fetchall()
let_data_group = []
for row in results_role:
    as_list = list(row)
    as_list[5] = 1
    as_list[7] = None
    let_data_group.append(tuple(as_list))

print(let_data_group)
try:
    cur_his_new.executemany(sql_group_insert, let_data_group)
    conn_his_new.commit()
except:
    print("errrpr")

# uset

sql_user_select = 'select hu.username, hu.password, hu.password_algorithm, hu.code, hu.fullname, hu.fullname_search, ' \
                  'hu.email, hu.phone, ' \
                  'hu.title, hd.code, hu.`type`, hu.enabled, hu.hospital_id, hu.created_time, hu.creator_id,' \
                  ' hu.last_updated_time, ' \
                  'hu.last_updated_id, hu.voided, hu.voided_time, hu.voided_by, hu.void_reason from hosp_user hu ' \
                  'left join hosp_department hd on hu.department_id =hd.id'

sql_select_department_new = "select  id,code, hospital_id from hosp_department "
sql_user_insert = "INSERT INTO hosp_user" \
                  "( username, password, password_algorithm, code, fullname, fullname_search, email, phone, title," \
                  "department_id, " \
                  "`type`, enabled, hospital_id, created_time, creator_id, last_updated_time, last_updated_id, voided," \
                  "voided_time, voided_by, void_reason)" \
                  "VALUES( %s, %s, %s, %s, %s, " \
                  "%s, " \
                  "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s," \
                  "%s, %s, %s); "

# select department
cur_his_new.execute(sql_select_department_new)
results_department_new = cur_his_new.fetchall()
data_department_new = {}
for row in results_department_new:
    data_department_new[row[1]] = row

cur_his.execute(sql_user_select)
results_user = cur_his.fetchall()
let_data_user = []
print(json.dumps(data_department_new))
for row in results_user:
    as_list = list(row)

    if data_department_new.get(row[9]) is not None and data_department_new[row[9]][2] == row[12]:
        as_list[9] = data_department_new[row[9]][0]
        as_list[14] = 1
        as_list[16] = None
        as_list[19] = None
        let_data_user.append(tuple(as_list))
print(let_data_user[2][9])
print(let_data_user[1][9])

let_data_user.remove(let_data_user[2])
try:
    cur_his_new.executemany(sql_user_insert, let_data_user)
    conn_his_new.commit()
except:
    print("errrpr")

# uset


# role_grop

sql_role_group = "select hg.hospital_id,hg.name,hrg.role_id from hosp_group_role hrg left join hosp_group " \
                 "hg on  hrg.group_id= hg.id"

sql_role_group_select = "INSERT INTO hosp_group_role" \
                        "(group_id, role_id)" \
                        "VALUES(%s,%s);"

sql_group_new = "select id,hospital_id,name from hosp_group"

cur_his_new.execute(sql_group_new)
results_group_new = cur_his_new.fetchall()
data_group_new = {}
for row in results_group_new:
    data_group_new[row[1] + row[2]] = row[0]
print(data_group_new)

cur_his.execute(sql_role_group)
results_role_group = cur_his.fetchall()
let_data_role_group_1 = []
for row in results_role_group:
    data_insert = []
    if data_group_new.get(row[0] + row[1]) is not None:
        data_insert.append(data_group_new[row[0] + row[1]])
        data_insert.append(row[2])

    let_data_role_group_1.append(tuple(data_insert))
try:
    cur_his_new.executemany(sql_role_group_select, let_data_role_group_1)
    conn_his_new.commit()
except:
    print("errrpr")

# user role

sql_user_group_select = "select hg.id,hg.hospital_id,hg.name ,hu.username from hosp_group_user hgu left join hosp_user" \
                        " hu on  hu.id= hgu.user_id left join hosp_group hg on hg.id= hgu.group_id"

sql_select_user_new = 'select id,username from hosp_user'
sql_user_group_insert = 'INSERT INTO hosp_group_user' \
                        '(user_id, group_id)' \
                        'VALUES(%s, %s);'
cur_his_new.execute(sql_select_user_new)
results_user_new = cur_his_new.fetchall()
data_user_new = {}
for row in results_user_new:
    data_user_new[row[1]] = row[0]

cur_his.execute(sql_user_group_select)
results_user_group = cur_his.fetchall()
let_data_user_group = []
print(results_user_group)
for row in results_user_group:
    data_insert = []
    if data_user_new.get(row[3]) is not None and data_group_new.get(row[1] + row[2]) is not None:
        data_insert.append(data_user_new[row[3]])
        data_insert.append(data_group_new[row[1] + row[2]])
        let_data_user_group.append(tuple(data_insert))
try:
    cur_his_new.executemany(sql_user_group_insert, let_data_user_group)
    conn_his_new.commit()
except:
    print("errrpr")

# uset


# modality group

sql_modality_group_select = "select  code, description, hospital_id, created_time, creator_id, " \
                            "last_updated_time, last_updated_id " \
                            " from  hosp_modality_group"

sql_modality_group_insert = "INSERT INTO hosp_modality_group" \
                            "(code, description, hospital_id, created_time, creator_id, " \
                            "last_updated_time, last_updated_id)" \
                            "VALUES( %s,%s,%s,%s,%s,%s,%s);"
cur_his.execute(sql_modality_group_select)
results_modality_group = cur_his.fetchall()
let_data_modality_group = []
print(results_user_group)
for row in results_modality_group:
    as_list = list(row)
    as_list[4] = 0
    as_list[6] = 0
    let_data_modality_group.append(tuple(as_list))
print(let_data_modality_group)

try:
    cur_his_new.executemany(sql_modality_group_insert, let_data_modality_group)
    conn_his_new.commit()
except:
    print("errrpr")

# # uset
#
#
# modality


sql_modality_select = "select   concat(hmg.hospital_id,hmg.code), hm.code, hm.name, hm.modality_type, " \
                      "hm.room_no, hm.dicom_supported, " \
                      "hm.institution_name," \
                      " hm.model_name, hm.station_name, hm.ae_title, hm.ip_address, hm.virtual, hm.enabled," \
                      " hm.hospital_id," \
                      "hm.created_time, hm.creator_id, hm.last_updated_time, hm.last_updated_id, hm.voided," \
                      " hm.voided_time, hm.voided_by," \
                      " hm.void_reason" \
                      " from  hosp_modality hm left join hosp_modality_group hmg on hmg.id=hm.group_id"

sql_modality_insert = "INSERT INTO hosp_modality" \
                      "( group_id, code, name, modality_type, room_no, dicom_supported, institution_name, model_name, " \
                      "station_name, ae_title, ip_address, virtual, enabled, hospital_id, created_time, " \
                      "creator_id, last_updated_time, last_updated_id, voided, voided_time, voided_by, void_reason)" \
                      "VALUES(%s,%s,%s,%s,%s,%s,%s, %s," \
                      "%s,%s,%s,%s,%s,%s,%s,%s, " \
                      "%s,%s,%s,%s,%s,%s);"

sql_modality_group_new_select = 'select id,hospital_id,code from hosp_modality_group'
cur_his_new.execute(sql_modality_group_new_select)
results_modality_group_new = cur_his_new.fetchall()
data_modality_group_new = {}
for row in results_modality_group_new:
    data_modality_group_new[row[1] + row[2]] = row[0]

print(data_modality_group_new)
cur_his.execute(sql_modality_select)
results_modality = cur_his.fetchall()
let_data_modality = []
for row in results_modality:
    as_list = list(row)
    as_list[0] = data_modality_group_new.get(row[0])
    as_list[15] = 0
    as_list[17] = None
    as_list[19] = None
    let_data_modality.append(tuple(as_list))
print(let_data_modality)

try:
    cur_his_new.executemany(sql_modality_insert, let_data_modality)
    conn_his_new.commit()
except:
    print("errrpr")

# # uset
#
#
# modality_type

sql_modality_type_select = "select id, abbr_id, preferred_modality_id,  config_json," \
                           " hospital_id, created_time, creator_id, last_updated_time, last_updated_id" \
                           " from hosp_modality_type"

sql_modality_type_insert = 'INSERT INTO hosp_modality_type' \
                           "(id,abbr_id, preferred_modality_id, config_json, hospital_id," \
                           " created_time, creator_id, last_updated_time, last_updated_id,scheduled_type)" \
                           "VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"

sql_modality_select_new = 'select id,hospital_id,modality_type from hosp_modality'

cur_his_new.execute(sql_modality_select_new)
results_modality_new = cur_his_new.fetchall()
data_modality_new = {}
for row in results_modality_new:
    data_modality_new[row[1] + "-" + row[2]] = row[0]

print(data_modality_new)
cur_his.execute(sql_modality_type_select)
results_modality_type = cur_his.fetchall()
let_data_modality_type = []
print(len(results_modality_type))
for row in results_modality_type:
    as_list = list(row)
    if data_modality_new.get(row[4] + "-" + row[1]) :
        as_list[0]=row[4]+"-"+row[1]
        as_list[2] = data_modality_new[row[4] + "-" + row[1]]
        as_list[6] = 0
        as_list[8] = None
        as_list.append('NONE')
        let_data_modality_type.append(tuple(as_list))
        print(as_list[0])
print(let_data_modality_type)

# try:
cur_his_new.executemany(sql_modality_type_insert, let_data_modality_type)
conn_his_new.commit()
# except:
#     print("errrpr")

# uset

#
# # modality_type
